﻿using System;
using System.Threading.Tasks;

using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Moq;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Implementations;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Context.Implementations;
using Services.Payments.PayIt.Infrastructure.Context.Interfaces;
using Services.Payments.PayIt.Infrastructure.Models.Request;

namespace Services.Payments.PayIt.Api.Tests
{
    public class BaseTestSetup
    {
        #region Properties 

        private readonly ServiceProvider serviceProvider;
        public readonly IDbContext dbContext;
        public readonly NatWestPayItConfigurationResource natwestPayItSettings;
        private Database _cosmosDatabase;

        #endregion Properties

        #region Constructors

        public BaseTestSetup()
        {
            serviceProvider = this.LoadServiceProvider();
            dbContext = new DbContext(this.GetMockLogger<DbContext>(), serviceProvider.GetService<Database>());
            natwestPayItSettings = serviceProvider.GetService<NatWestPayItConfigurationResource>();
        }

        #endregion Constructors

        private ServiceProvider LoadServiceProvider()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            Task.Run(async () =>
            {
                _cosmosDatabase = await new CosmosClient(configuration.GetSection("Database:ConnectionString").Value)
                    .CreateDatabaseIfNotExistsAsync(configuration.GetSection("Database:DatabaseName").Value);
            }).Wait();

            ServiceCollection services = new ServiceCollection();

            services.AddSingleton(_cosmosDatabase);
            services.AddSingleton(provider => configuration.GetSection("ApplicationInsights").Get<ApplicationInsightsSettingsResource>());
            services.AddSingleton(provider => configuration.GetSection("NatWestPayItConfiguration").Get<NatWestPayItConfigurationResource>());

            services.AddScoped<IDbContext, DbContext>();
            services.AddScoped<ITransactionsService, TransactionsService>();
            services.AddHttpClient<IPayItService, PayItService>();
            services.AddTransient<IWebhookService, WebhookService>();

            return services.BuildServiceProvider();
        }

        public ILogger<T> GetMockLogger<T>()
        {
            return new Mock<ILogger<T>>().Object;
        }

        #region Dummy Model

        public PaymentRequestModel DummyPaymentRequest => new PaymentRequestModel()
        {
            AccountNumber = "0000000000000000000",
            Amount = 0.01,
            Reason = "000000000000200621",
            RedirectUrl = natwestPayItSettings.RedirectionUrl
        };

        #endregion
    }
}
