using System;
using System.Linq;
using System.Threading.Tasks;

using Services.Payments.PayIt.Api.Tests.Controllers;
using Services.Payments.PayIt.Domain.Services.Implementations;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;

using Xunit;

namespace Services.Payments.PayIt.Api.Tests.Services
{
    public class TransactionServiceTests : BaseTestSetup
    {
        #region Properties

        private readonly TransactionsService _transactionsService;

        #endregion Properties

        #region Constructors

        public TransactionServiceTests()
        {
            _transactionsService = new TransactionsService(this.GetMockLogger<TransactionsService>(), this.dbContext);
            Task.Run(async() => await new PaymentsControllerTests().Test_InitiatePayment()).Wait();
        }

        #endregion Constructors

        [Fact]
        public async Task Test_GetPayments()
        {
            var payments = await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber);

            Assert.NotNull(payments);
            Assert.True(payments.Count > 0);
        }

        [Fact]
        public async Task Test_UpdatePayment()
        {
            var payment = (await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber)).FirstOrDefault();

            Assert.NotNull(payment);

            var paymentConfirmationData = new PaymentConfirmationData
            {
                PaymentReferenceId = payment.PaymentReferenceId ?? Guid.NewGuid().ToString().ToLower().Remove(35, 1),
                TppReferenceId = payment.TppReferenceId,
                Status = PaymentStatus.PaymentNotProcessed,
                StatusDateTime = DateTimeOffset.UtcNow
            };

            var updatedPayment = await _transactionsService.UpdatePayment(payment.TransactionId, paymentConfirmationData);

            Assert.NotNull(updatedPayment);
            Assert.True(updatedPayment.PaymentReferenceId == paymentConfirmationData.PaymentReferenceId);
            Assert.True(updatedPayment.TppReferenceId == paymentConfirmationData.TppReferenceId);
            Assert.True(updatedPayment.Status == paymentConfirmationData.Status.ToString());
            Assert.True(updatedPayment.UpdatedDateTime == paymentConfirmationData.StatusDateTime);

            paymentConfirmationData.Status = PaymentStatus.PaymentApproved;
            paymentConfirmationData.StatusDateTime = DateTimeOffset.UtcNow;

            updatedPayment = await _transactionsService.UpdatePayment(payment.TransactionId, paymentConfirmationData);

            Assert.NotNull(updatedPayment);
            Assert.True(updatedPayment.Status == paymentConfirmationData.Status.ToString());
            Assert.True(updatedPayment.UpdatedDateTime == paymentConfirmationData.StatusDateTime);
        }

        [Fact]
        public async Task Test_GetPayment()
        {
            var payment = (await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber)).FirstOrDefault();

            Assert.NotNull(payment);

            var fetchedPayment = await _transactionsService.GetPayment(payment.TransactionId);

            Assert.NotNull(fetchedPayment);
            Assert.True(fetchedPayment.TransactionId == payment.TransactionId);
        }
    }
}
