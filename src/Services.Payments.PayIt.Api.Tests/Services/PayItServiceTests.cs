using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using Services.Payments.PayIt.Domain.Services.Implementations;

using Xunit;

namespace Services.Payments.PayIt.Api.Tests.Services
{
    public class PayItServiceTests : BaseTestSetup
    {
        #region Properties

        private readonly PayItService _payItService;

        #endregion Properties

        #region Constructors

        public PayItServiceTests()
        {
            _payItService = new PayItService(new HttpClient(), this.GetMockLogger<PayItService>(), this.dbContext, this.natwestPayItSettings);
        }

        #endregion Constructors

        [Fact]
        public async Task Test_GetAuthentication()
        {
            var token = await _payItService.GetAuthentication();

            Assert.NotNull(token);
            Assert.True(!string.IsNullOrWhiteSpace(token.AccessToken));
        }

        [Fact]
        public async Task Test_GetEligibleBanks()
        {
            var eligibleBanks = await _payItService.GetEligibleBanks();

            Assert.NotNull(eligibleBanks);
            Assert.True(eligibleBanks.Count > 0);
        }

        [Fact]
        public async Task Test_InitiatePayment()
        {
            _payItService.AddFovIndicator(new DefaultHttpContext(), null);

            var paymentRequestResponse = await _payItService.InitiatePayment(DummyPaymentRequest);

            Assert.NotNull(paymentRequestResponse);
            Assert.NotNull(paymentRequestResponse.Data);
            Assert.True(!string.IsNullOrWhiteSpace(paymentRequestResponse.Data.ResponseUrl));
        }
    }
}
