﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Services.Payments.PayIt.Api.Controllers;
using Services.Payments.PayIt.Domain.Services.Implementations;

using Xunit;

namespace Services.Payments.PayIt.Api.Tests.Controllers
{
    public class TransactionsControllerTests : BaseTestSetup
    {
        #region Properties

        private readonly TransactionsService _transactionsService;
        private readonly TransactionsController _transactionsController;

        #endregion Properties

        #region Constructors

        public TransactionsControllerTests()
        {
            _transactionsService = new TransactionsService(this.GetMockLogger<TransactionsService>(), this.dbContext);
            _transactionsController = new TransactionsController(this.GetMockLogger<TransactionsController>(), _transactionsService);
            _transactionsController.ControllerContext = new ControllerContext() { HttpContext = new DefaultHttpContext() };
            Task.Run(async () => await new PaymentsControllerTests().Test_InitiatePayment()).Wait();
        }

        #endregion Constructors

        [Fact]
        public async Task Test_GetTransaction()
        {
            var transaction = (await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber)).FirstOrDefault();

            Assert.NotNull(transaction);

            var result = await _transactionsController.Get(transaction.AccountNumber, transaction.TransactionId);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_GetTransactions()
        {
            var result = await _transactionsController.Get(DummyPaymentRequest.AccountNumber);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }
    }
}
