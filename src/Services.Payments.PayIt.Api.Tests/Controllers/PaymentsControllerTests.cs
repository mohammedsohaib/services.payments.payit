﻿using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Services.Payments.PayIt.Api.Controllers;
using Services.Payments.PayIt.Domain.Services.Implementations;
using Services.Payments.PayIt.Infrastructure.Models.Request;

using Xunit;

namespace Services.Payments.PayIt.Api.Tests.Controllers
{
    public class PaymentsControllerTests : BaseTestSetup
    {
        #region Properties

        private readonly PaymentsController _paymentsController;

        #endregion Properties

        #region Constructors

        public PaymentsControllerTests()
        {
            _paymentsController = new PaymentsController(this.GetMockLogger<PaymentsController>(),
                new PayItService(new HttpClient(), this.GetMockLogger<PayItService>(), this.dbContext, this.natwestPayItSettings),
                this.natwestPayItSettings);
            _paymentsController.ControllerContext = new ControllerContext() { HttpContext = new DefaultHttpContext() };
        }

        #endregion Constructors

        [Fact]
        public async Task Test_EligibleBanks()
        {
            var result = await _paymentsController.EligibleBanks();
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task Test_InitiatePayment()
        {
            var paymentInitiationModel = new PaymentInitiationModel
            {
                AccountNumber = DummyPaymentRequest.AccountNumber,
                Amount = DummyPaymentRequest.Amount
            };

            var result = await _paymentsController.InitiatePayment(paymentInitiationModel);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }
    }
}

