﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using Newtonsoft.Json;

using Services.Payments.PayIt.Api.Controllers;
using Services.Payments.PayIt.Domain.Services.Implementations;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;
using Services.Payments.PayIt.Infrastructure.Models.Response;

using Xunit;

namespace Services.Payments.PayIt.Api.Tests.Controllers
{
    public class ConfirmationControllerTests : BaseTestSetup
    {
        #region Properties

        private readonly WebhookService _webhookService;
        private readonly TransactionsService _transactionsService;
        private readonly ConfirmationController _confirmationController;

        #endregion Properties

        #region Constructors

        public ConfirmationControllerTests()
        {
            _webhookService = new WebhookService(this.GetMockLogger<WebhookService>(), this.natwestPayItSettings);
            _transactionsService = new TransactionsService(this.GetMockLogger<TransactionsService>(), this.dbContext);
            _confirmationController = new ConfirmationController(this.GetMockLogger<ConfirmationController>(), _webhookService, _transactionsService, this.natwestPayItSettings);
            _confirmationController.ControllerContext = new ControllerContext() { HttpContext = new DefaultHttpContext() };
        }

        #endregion Constructors

        [Fact]
        public async Task Test_Get()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.QueryString = new QueryString("?s=UGF5bWVudEFwcHJvdmVk&e=OTJlYmMxZGJmYjhkNGEzYmEwMmQwYTlhYWRiYTIyODA=&a=MTAuMDA=&t=YzdhMjM1ZGRjODE0NGFhOGJjOTAxMWY5YTdkODljODU4ODg=");
            _confirmationController.ControllerContext = new ControllerContext() { HttpContext = httpContext };

            var result = await _confirmationController.Get();

            Assert.NotNull(result);
            Assert.True(result.ContentType == "text/html");
            Assert.NotNull(result.Content);
        }

        [Fact]
        public async Task Test_Post()
        {
            // Create an empty payment for testing purposes
            await new PaymentsControllerTests().Test_InitiatePayment();

            var transaction = (await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber))
                .Where(p => string.IsNullOrWhiteSpace(p.Status)).OrderByDescending(p => p.CreationDateTime)
                .FirstOrDefault();

            Assert.NotNull(transaction);

            var payload = JsonConvert.SerializeObject(new PaymentConfirmationModel
            {
                PaymentUpdate = new PaymentConfirmationData
                {
                    PaymentReferenceId = transaction.PaymentReferenceId ?? Guid.NewGuid().ToString().ToLower().Remove(35, 1),
                    Status = PaymentStatus.PaymentApproved,
                    StatusDateTime = DateTimeOffset.UtcNow,
                    TppReferenceId = transaction.TppReferenceId
                }
            });

            _confirmationController.HttpContext.Request.Headers.Add("payit-signature", await GetPayItSignature(payload));
            _confirmationController.HttpContext.Request.Headers.Add("x-transaction-ID", transaction.TransactionId);
            _confirmationController.HttpContext.Request.Body = await CovertStringToStream(payload);

            var result = (await _confirmationController.Post()) as OkObjectResult;

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task Test_Patch()
        {
            // Create an empty payment for testing purposes
            await new PaymentsControllerTests().Test_InitiatePayment();

            var transaction = (await _transactionsService.GetPayments(DummyPaymentRequest.AccountNumber))
                .Where(p => string.IsNullOrWhiteSpace(p.Status)).OrderByDescending(p => p.CreationDateTime)
                .FirstOrDefault();

            Assert.NotNull(transaction);

            var payload = JsonConvert.SerializeObject(new PaymentConfirmationPatchModel
            {
                Status = PaymentStatus.PaymentApproved
            });

            _confirmationController.HttpContext.Request.Headers.Add("payit-signature", await GetPayItSignature(payload));
            _confirmationController.HttpContext.Request.Headers.Add("x-transaction-ID", transaction.TransactionId);
            _confirmationController.HttpContext.Request.Body = await CovertStringToStream(payload);

            var result = (await _confirmationController.Patch(transaction.PaymentReferenceId ?? Guid.NewGuid().ToString().ToLower().Remove(35, 1), transaction.TppReferenceId)) as OkObjectResult;

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact(Skip = "This is used locally when debugging confirmation controller to generate the payit-signature")]
        public async Task Test_GeneratePayItSignature()
        {
            var payload = JsonConvert.SerializeObject(new
            {
                PaymentUpdate = new
                {
                    PaymentReferenceId = "5962bd15-2c91-4dec-a25c-65df3f31b331",
                    Status = PaymentStatus.PaymentApproved.ToString(),
                    StatusDateTime = DateTimeOffset.UtcNow,
                    TppReferenceId = "6a2d13567ebe4e948603909c66cded2a"
                }
            });

            var signature = await GetPayItSignature(payload);

            Assert.NotNull(signature);
        }

        #region Helper Methods

        private async Task<Stream> CovertStringToStream(string payload)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(payload);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }

        private async Task<string> GetPayItSignature(string payload)
        {
            var timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            string signedPayload = string.Concat(timestamp.ToString(), '.', payload);

            var clientSecret = natwestPayItSettings.ClientSecret;

            if (natwestPayItSettings.IsSandbox)
            {
                clientSecret = natwestPayItSettings.SandboxStaticClientSecret;
            }

            var hmac = new HMACSHA256(Encoding.ASCII.GetBytes(clientSecret));
            var generatedSignature = Convert.ToBase64String(hmac.ComputeHash(Encoding.ASCII.GetBytes(signedPayload)));

            return $"t={timestamp},v1={generatedSignature}";
        }

        #endregion Helper Methods
    }
}