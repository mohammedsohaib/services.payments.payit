﻿namespace Services.Payments.PayIt.Core.Resources
{
    public class ApplicationInsightsSettingsResource
    {
        public bool DeveloperMode { get; set; }
        public string InstrumentationKey { get; set; }
        public bool EnableAdaptiveSampling { get; set; }
        public string ExcludedPaths { get; set; }
    }
}
