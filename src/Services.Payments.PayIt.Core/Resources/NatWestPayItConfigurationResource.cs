﻿namespace Services.Payments.PayIt.Core.Resources
{
    public class NatWestPayItConfigurationResource
    {
        public string TenantId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string GrantType { get; set; }
        public string ApiBaseUrl { get; set; }
        public string CompanyId { get; set; }
        public string BrandId { get; set; }
        public string FovIndicator { get; set; }
        public bool IsSandbox { get; set; }
        public string SandboxStaticClientSecret { get; set; }
        public string RedirectionUrl { get; set; }
    }
}
