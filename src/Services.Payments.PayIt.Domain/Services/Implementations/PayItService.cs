﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Context.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;
using Services.Payments.PayIt.Infrastructure.Models.Response;

namespace Services.Payments.PayIt.Domain.Services.Implementations
{
    public class PayItService : IPayItService
    {
        #region Properties

        private readonly NatWestPayItConfigurationResource _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<PayItService> _logger;
        private readonly IDbContext _dbContext;

        #endregion Properties

        #region Constructors

        public PayItService(HttpClient httpClient, ILogger<PayItService> logger, IDbContext dbContext, NatWestPayItConfigurationResource natWestPayItConfiguration)
        {
            _httpClient = httpClient;
            _logger = logger;
            _dbContext = dbContext;
            _settings = natWestPayItConfiguration;

            this.SetupHttpClient();
        }

        #endregion Constructors

        public async Task<AuthenticationToken> GetAuthentication()
        {
            string endpoint = $"https://login.microsoftonline.com/{_settings.TenantId}/oauth2/token";

            var form = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                {"grant_type", _settings.GrantType},
                {"client_id", _settings.ClientId},
                {"client_secret", _settings.ClientSecret}
            });

            HttpResponseMessage httpResponseMessage = await new HttpClient().PostAsync(endpoint, form);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                var error = JsonConvert.DeserializeObject<dynamic>(await httpResponseMessage.Content.ReadAsStringAsync())?.message;
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(GetAuthentication), $"The request to NatWest PayIt returned the response code {httpResponseMessage.StatusCode}: {error}");
            }

            var responseJsonString = await httpResponseMessage.Content.ReadAsStringAsync();

            AuthenticationToken payItToken = JsonConvert.DeserializeObject<AuthenticationToken>(responseJsonString);
            if (payItToken == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(GetAuthentication), $"Returned null when attempting deserialisation to {payItToken.GetType().FullName}");
            }

            return payItToken;
        }

        public async Task<List<EligibleBank>> GetEligibleBanks()
        {
            string endpoint = $"{_settings.ApiBaseUrl.TrimEnd('/')}/eligible-banks/{_settings.CompanyId}/{_settings.BrandId}";

            HttpResponseMessage httpResponseMessage = await _httpClient.GetAsync(endpoint);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                var error = JsonConvert.DeserializeObject<dynamic>(await httpResponseMessage.Content.ReadAsStringAsync())?.message;
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(GetEligibleBanks), $"The request to NatWest PayIt returned the response code {httpResponseMessage.StatusCode}: {error}");
            }

            var responseJsonString = await httpResponseMessage.Content.ReadAsStringAsync();

            List<EligibleBank> eligibleBanks = JsonConvert.DeserializeObject<List<EligibleBank>>(responseJsonString);
            if (eligibleBanks == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(GetEligibleBanks), $"Returned null when attempting deserialisation to {eligibleBanks.GetType().FullName}");
            }

            return eligibleBanks;
        }

        public async Task<PaymentRequestResponse> InitiatePayment(PaymentRequestModel paymentRequestModel)
        {
            string endpoint = $"{_settings.ApiBaseUrl.TrimEnd('/')}/merchant-payments";
            string transactionId = _httpClient.DefaultRequestHeaders.GetValues("x-transaction-ID").Single();

            PaymentRequest paymentRequest = new PaymentRequest
            {
                Data = new PaymentRequestData
                {
                    CompanyId = _settings.CompanyId,
                    BrandId = _settings.BrandId,
                    Amount = paymentRequestModel.Amount.ToString("N2").Replace(",", ""),
                    Reason = paymentRequestModel.Reason,
                    RedirectUrl = paymentRequestModel.RedirectUrl
                }
            };

            LoggingHelper.LogInformation(_logger, nameof(InitiatePayment), $"Initiating payment for TransactionId: {transactionId} with Amount: {paymentRequest.Data.Amount}, sent as value of: {paymentRequestModel.Amount}");

            var stringContent = new StringContent(JsonConvert.SerializeObject(paymentRequest), System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage httpResponseMessage = await _httpClient.PostAsync(endpoint, stringContent);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                var x = await httpResponseMessage.Content.ReadAsStringAsync();
                var error = JsonConvert.DeserializeObject<dynamic>(await httpResponseMessage.Content.ReadAsStringAsync())?.message;
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(InitiatePayment), $"The request to NatWest PayIt returned the response code {httpResponseMessage.StatusCode}: {error}");
            }

            var responseJsonString = await httpResponseMessage.Content.ReadAsStringAsync();

            PaymentRequestResponse paymentRequestResponse = JsonConvert.DeserializeObject<PaymentRequestResponse>(responseJsonString);
            if (paymentRequestResponse == null || paymentRequestResponse.Data == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(InitiatePayment), $"Returned null when attempting deserialisation to {paymentRequestResponse.GetType().FullName}");
            }

            // Set transaction id in response so it can be fetched later
            paymentRequestResponse.Data.TransactionId = transactionId;

            var pendingPayment = new Payment
            {
                TransactionId = transactionId,
                AccountNumber = paymentRequestModel.AccountNumber,
                TppReferenceId = paymentRequestResponse.Data.TppReferenceId,
                Currency = paymentRequest.Data.Currency,
                Amount = paymentRequestModel.Amount.ToString("N2"),
                Reason = paymentRequest.Data.Reason,
                CreationDateTime = paymentRequestResponse.Data.CreationDateTime,
            };

            await _dbContext.Insert(pendingPayment);

            return paymentRequestResponse;
        }


        #region Helper Methods

        private void SetupHttpClient()
        {
            var task = Task.Run(async () =>
            {
                var token = await this.GetAuthentication();
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.TokenType, token.AccessToken);
                _httpClient.DefaultRequestHeaders.Add("x-transaction-ID", System.Guid.NewGuid().ToString("N").ToLower().Remove(31, 1));
            });

            task.Wait();
        }

        public void AddFovIndicator(HttpContext httpContext, PaymentStatus? _paymentStatus)
        {
            var fovIndicator = string.Empty;

            if (_settings.IsSandbox)
            {
                if (httpContext == null) return;

                var apiUrl = WebUtility.UrlEncode($"{_settings.RedirectionUrl.TrimEnd('/')}{"/merchant-payments-confirm"}");
                var redirectUrl = WebUtility.UrlEncode(_settings.RedirectionUrl);
                var paymentStatus = _paymentStatus.HasValue ? _paymentStatus.ToString() : PaymentStatus.PaymentApproved.ToString();

                fovIndicator = $"api_url={apiUrl}&redirect_url={redirectUrl}&status={paymentStatus}";
            }
            else
            {
                fovIndicator = this._settings.FovIndicator;
            }

            _httpClient.DefaultRequestHeaders.Add("x-FOV-indicator", fovIndicator);
        }

        #endregion Helper Methods
    }
}
