﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;

namespace Services.Payments.PayIt.Domain.Services.Implementations
{
    public class WebhookService : IWebhookService
    {
        #region Properties

        private readonly NatWestPayItConfigurationResource _settings;
        private readonly ILogger<WebhookService> _logger;

        #endregion Properties

        #region Constructors

        public WebhookService(ILogger<WebhookService> logger, NatWestPayItConfigurationResource natWestPayItConfiguration)
        {
            _logger = logger;
            _settings = natWestPayItConfiguration;
        }

        #endregion Constructors

        public async Task<bool> VerifyPayload(string signature, string payload)
        {
            LoggingHelper.LogInformation(_logger, nameof(VerifyPayload), "Verifying payload");

            string payitTimestampString = (signature.Split(','))[0].Split("t=")[1];
            DateTimeOffset payitTimestamp = DateTimeOffset.FromUnixTimeMilliseconds(long.Parse(payitTimestampString));
            string payitSignature = (signature.Split(','))[1].Split("v1=")[1];

            string signedPayload = string.Concat(payitTimestampString, '.', payload);

            LoggingHelper.LogInformation(_logger, nameof(VerifyPayload), "Getting hashed signature");

            var clientSecret = _settings.ClientSecret;

            if (_settings.IsSandbox)
            {
                clientSecret = _settings.SandboxStaticClientSecret;
            }

            var hmac = new HMACSHA256(Encoding.ASCII.GetBytes(clientSecret));
            var generatedSignature = Convert.ToBase64String(hmac.ComputeHash(Encoding.ASCII.GetBytes(signedPayload)));

            bool signaturesMatch = generatedSignature == payitSignature;
            if (!signaturesMatch)
            {
                LoggingHelper.LogError(_logger, nameof(VerifyPayload), "Signatures do not match");
            }

            bool validTimestamp = (DateTimeOffset.UtcNow - payitTimestamp).TotalDays <= 7;
            if (!validTimestamp)
            {
                LoggingHelper.LogError(_logger, nameof(VerifyPayload), "Difference between current time cannot be more than 7 days");
            }

            return signaturesMatch && validTimestamp;
        }

    }
}
