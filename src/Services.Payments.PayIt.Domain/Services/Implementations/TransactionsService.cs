﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Context.Interfaces;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;

namespace Services.Payments.PayIt.Domain.Services.Implementations
{
    public class TransactionsService : ITransactionsService
    {
        #region Properties

        private readonly ILogger<TransactionsService> _logger;
        private readonly IDbContext _dbContext;

        #endregion Properties

        #region Constructors

        public TransactionsService(ILogger<TransactionsService> logger, IDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        #endregion Constructors

        public async Task<Payment> GetPayment(string transactionId)
        {
            return await _dbContext.Get<Payment>(transactionId);
        }

        public async Task<List<Payment>> GetPayments(string accountNumber)
        {
            return await _dbContext.Get<Payment>(typeof(Payment).GetProperty(nameof(Payment.AccountNumber)), accountNumber);
        }

        public async Task<Payment> UpdatePayment(string transactionId, PaymentConfirmationData paymentConfirmationData)
        {
            var payment = await _dbContext.Get<Payment>(transactionId);

            payment.PaymentReferenceId = paymentConfirmationData.PaymentReferenceId;
            payment.TppReferenceId = paymentConfirmationData.TppReferenceId;
            payment.Status = paymentConfirmationData.Status.ToString();
            payment.UpdatedDateTime = paymentConfirmationData.StatusDateTime;

            payment = await _dbContext.Update(payment);

            return payment;
        }
    }
}
