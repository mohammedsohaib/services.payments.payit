﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;
using Services.Payments.PayIt.Infrastructure.Models.Response;

namespace Services.Payments.PayIt.Domain.Services.Interfaces
{
    public interface IPayItService
    {
        Task<AuthenticationToken> GetAuthentication();

        Task<List<EligibleBank>> GetEligibleBanks();

        Task<PaymentRequestResponse> InitiatePayment(PaymentRequestModel paymentRequestModel);

        void AddFovIndicator(HttpContext httpContext, PaymentStatus? paymentStatus);
    }
}
