﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;

namespace Services.Payments.PayIt.Domain.Services.Interfaces
{
    public interface ITransactionsService
    {
        Task<Payment> GetPayment(string transactionId);

        Task<List<Payment>> GetPayments(string accountNumber);

        Task<Payment> UpdatePayment(string transactionId, PaymentConfirmationData paymentConfirmationData);
    }
}
