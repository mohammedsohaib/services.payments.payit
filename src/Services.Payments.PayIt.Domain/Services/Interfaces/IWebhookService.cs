﻿using System.Threading.Tasks;

namespace Services.Payments.PayIt.Domain.Services.Interfaces
{
    public interface IWebhookService
    {
        Task<bool> VerifyPayload(string signature, string payload);
    }
}
