﻿namespace Services.Payments.PayIt.Infrastructure.Models.Interfaces
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
