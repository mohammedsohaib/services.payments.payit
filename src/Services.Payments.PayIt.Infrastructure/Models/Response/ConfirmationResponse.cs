﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class ConfirmationResponse
    {
        [JsonProperty("Data")]
        public ConfirmationResponseData Data { get; set; }
    }
}
