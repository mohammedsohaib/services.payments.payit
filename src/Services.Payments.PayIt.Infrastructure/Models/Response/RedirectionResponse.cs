﻿namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class RedirectionResponse
    {
        public string Status { get; set; }

        public string PaymentAmount { get; set; }
    }
}
