﻿using System;

using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class EligibleBank
    {
        public string BankName { get; set; }

        public Uri LogoUrl { get; set; }
    }
}