﻿using System;

using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class PaymentRequestResponseData
    {
        public string TransactionId { get; set; }

        public string ResponseUrl { get; set; }

        public string TppReferenceId { get; set; }

        public DateTimeOffset CreationDateTime { get; set; }
    }
}
