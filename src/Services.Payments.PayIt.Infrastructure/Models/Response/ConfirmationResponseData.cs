﻿using System;

using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class ConfirmationResponseData
    {
        [JsonProperty("CreationDateTime")]
        public string CreationDateTime { get; set; }

        [JsonProperty("MerchantAcknowledgementId")]
        public string MerchantAcknowledgementId { get; set; }
    }
}
