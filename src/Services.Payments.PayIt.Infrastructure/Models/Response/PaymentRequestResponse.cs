﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Response
{
    public class PaymentRequestResponse
    {
        public PaymentRequestResponseData Data { get; set; }
    }
}
