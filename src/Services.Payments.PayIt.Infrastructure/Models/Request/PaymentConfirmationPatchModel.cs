﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentConfirmationPatchModel
    {
        [JsonProperty("Status")]
        public PaymentStatus Status { get; set; }
    }
}
