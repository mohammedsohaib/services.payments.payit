﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentRequest
    {
        [JsonProperty("Data")]
        public PaymentRequestData Data { get; set; }
    }
}
