﻿using System;

using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentConfirmationData
    {
        [JsonProperty("PaymentReferenceID")]
        public string PaymentReferenceId { get; set; }

        [JsonProperty("Status")]
        public PaymentStatus Status { get; set; }

        [JsonProperty("StatusDateTime")]
        public DateTimeOffset StatusDateTime { get; set; }

        [JsonProperty("TPPReferenceID")]
        public string TppReferenceId { get; set; }
    }
}
