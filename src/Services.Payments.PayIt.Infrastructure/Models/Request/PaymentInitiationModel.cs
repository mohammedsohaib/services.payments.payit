﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentInitiationModel
    {
        [JsonRequired]
        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonRequired]
        [JsonProperty("amount")]
        public double Amount { get; set; }
    }
}
