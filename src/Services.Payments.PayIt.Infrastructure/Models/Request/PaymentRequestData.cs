﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentRequestData
    {
        [JsonProperty("companyId"), JsonRequired]
        public string CompanyId { get; set; }

        [JsonProperty("brandId"), JsonRequired]
        public string BrandId { get; set; }

        [JsonProperty("amount"), JsonRequired]
        public string Amount { get; set; }

        [JsonProperty("reason"), JsonRequired]
        public string Reason { get; set; }

        [JsonProperty("currency"), JsonRequired]
        public string Currency { get => "GBP"; }

        [JsonProperty("country"), JsonRequired]
        public string Country { get => "UK"; }

        [JsonProperty("redirectUrl")]
        public string RedirectUrl { get; set; }
    }
}
