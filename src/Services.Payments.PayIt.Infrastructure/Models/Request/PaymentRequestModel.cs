﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentRequestModel
    {
        [JsonRequired]
        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonRequired]
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("redirectUrl")]
        public string RedirectUrl { get; set; }

        [JsonRequired]
        [JsonProperty("reason")]
        public string Reason { get; set; }

        #region Constructors

        public PaymentRequestModel() { }

        public PaymentRequestModel(PaymentInitiationModel initiatePaymentModel)
        {
            AccountNumber = initiatePaymentModel.AccountNumber;
            Amount = initiatePaymentModel.Amount;
        }

        #endregion Constructors
    }
}
