﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Request
{
    public class PaymentConfirmationModel
    {
        [JsonProperty("PaymentUpdate")]
        public PaymentConfirmationData PaymentUpdate { get; set; }
    }
}
