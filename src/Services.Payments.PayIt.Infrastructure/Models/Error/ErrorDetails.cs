﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models.Error
{
    public class ErrorDetails
    {
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
