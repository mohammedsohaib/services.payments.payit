﻿using Newtonsoft.Json;

namespace Services.Payments.PayIt.Infrastructure.Models
{
    public class Entity : Interfaces.IEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
