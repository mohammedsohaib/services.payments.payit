﻿using System;

namespace Services.Payments.PayIt.Infrastructure.Models
{
    [Flags]
    public enum PaymentStatus
    {
        Undefined,
        PaymentApproved,
        PaymentRejected,
        PaymentNotProcessed,
        PaymentStatusUnknown,
        PaymentAwaitingApproval,
        CustomerAbortedJourney,
        TechnicalError,
        Cancel
    }
}
