﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Payments.PayIt.Infrastructure.Models
{
    public class Payment : Entity
    {
        [Key]
        public string TransactionId { get; set; }

        public string AccountNumber { get; set; }

        public string TppReferenceId { get; set; }

        public string Currency { get; set; }

        public string Amount { get; set; }

        public string Reason { get; set; }

        public string PaymentReferenceId { get; set; }

        public string Status { get; set; }

        public DateTimeOffset CreationDateTime { get; set; }

        public DateTimeOffset? UpdatedDateTime { get; set; }
    }
}
