﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Logging;

using Services.Payments.PayIt.Infrastructure.Context.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;
using Services.Payments.PayIt.Infrastructure.Models.Interfaces;

namespace Services.Payments.PayIt.Infrastructure.Context.Implementations
{
    public class DbContext : IDbContext
    {
        #region Properties

        private readonly ILogger<DbContext> _logger;
        private readonly Database _cosmosDatabase;

        #endregion Properties

        #region Constructors

        public DbContext(ILogger<DbContext> logger, Database cosmosDatabase)
        {
            _logger = logger;
            _cosmosDatabase = cosmosDatabase;
        }

        #endregion Constructors

        public async Task<T> Insert<T>(T entity) where T : class, IEntity
        {
            var partitionKeyProperty = FetchPartitionKeyProperty<T>();
            var partitionKeyValue = partitionKeyProperty.GetValue(entity);

            entity.Id = partitionKeyValue.ToString();

            var response = await (await FetchContainer<T>()).CreateItemAsync<T>(entity, new PartitionKey(partitionKeyValue.ToString()));
            return response.Resource;
        }

        public async Task<T> Get<T>(dynamic id) where T : class, IEntity
        {
            var partitionKey = Convert.ChangeType(id, FetchPartitionKeyProperty<T>().PropertyType);

            var response = await (await FetchContainer<T>()).ReadItemAsync<T>(id, new PartitionKey(id));
            return response.Resource;
        }

        public async Task<List<T>> Get<T>(PropertyInfo propertyInfo, dynamic value) where T : class, IEntity
        {
            var query = new QueryDefinition($"SELECT * FROM c WHERE c.{propertyInfo.Name} = @value")
                .WithParameter("@value", Convert.ChangeType(value, propertyInfo.PropertyType));

            var items = new List<T>();
            var feedIterator = (await FetchContainer<T>()).GetItemQueryIterator<T>(query);

            while (feedIterator.HasMoreResults)
            {
                FeedResponse<T> response = await feedIterator.ReadNextAsync();
                foreach (var item in response)
                {
                    items.Add(item);
                }
            }

            return items;
        }

        public async Task<T> Update<T>(T entity) where T : class, IEntity
        {
            var partitionKeyProperty = FetchPartitionKeyProperty<T>();
            var partitionKeyValue = partitionKeyProperty.GetValue(entity);

            var response = await (await FetchContainer<T>()).ReplaceItemAsync<T>(entity, entity.Id, new PartitionKey(partitionKeyValue.ToString()));
            return response.Resource;
        }

        #region Helper Methods

        private async Task<Container> FetchContainer<T>()
        {
            var containerName = $"{typeof(T).Name}s";

            var partitionKeyProperty = FetchPartitionKeyProperty<T>();

            var response = await _cosmosDatabase.CreateContainerIfNotExistsAsync(new ContainerProperties
            {
                Id = containerName,
                PartitionKeyPath = $"/{partitionKeyProperty.Name}",
                DefaultTimeToLive = 604800
            });

            if (response?.Container == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(FetchContainer), "Failed to create/fetch container");
            }

            return response.Container;
        }

        private PropertyInfo FetchPartitionKeyProperty<T>()
        {
            var partitionKeyProperty = typeof(T)
                .GetProperties()
                .SingleOrDefault(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));

            if (partitionKeyProperty == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(FetchPartitionKeyProperty), $"{nameof(System.ComponentModel.DataAnnotations.KeyAttribute)} must be implemented in {typeof(T).FullName}");
            }

            if (partitionKeyProperty.PropertyType != typeof(string))
            {
                LoggingHelper.LogErrorAndThrowException(_logger, nameof(FetchPartitionKeyProperty), $"{nameof(System.ComponentModel.DataAnnotations.KeyAttribute)} can only be used once in {typeof(T).FullName}");
            }

            return partitionKeyProperty;
        }

        #endregion Helper Methods
    }
}