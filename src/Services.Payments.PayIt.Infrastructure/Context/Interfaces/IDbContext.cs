﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using Services.Payments.PayIt.Infrastructure.Models.Interfaces;

namespace Services.Payments.PayIt.Infrastructure.Context.Interfaces
{
    public interface IDbContext
    {
        Task<T> Insert<T>(T entity) where T : class, IEntity;

        Task<T> Get<T>(dynamic id) where T : class, IEntity;

        Task<List<T>> Get<T>(PropertyInfo propertyInfo, dynamic value) where T : class, IEntity;

        Task<T> Update<T>(T entity) where T : class, IEntity;
    }
}
