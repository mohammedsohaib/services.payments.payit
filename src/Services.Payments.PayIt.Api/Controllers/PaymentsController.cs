﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;
using Services.Payments.PayIt.Infrastructure.Models.Response;

namespace Services.Payments.PayIt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        #region Propeties

        private readonly NatWestPayItConfigurationResource _settings;
        private readonly ILogger<PaymentsController> _logger;
        private readonly IPayItService _payItService;

        #endregion Propeties

        #region Constructors

        public PaymentsController(ILogger<PaymentsController> logger, IPayItService payItService, NatWestPayItConfigurationResource natWestPayItConfiguration)
        {
            _logger = logger;
            _payItService = payItService;
            _settings = natWestPayItConfiguration;
        }

        #endregion Constructors

        /// <summary>
        /// Get eligible banks list.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ApiVersion("1.0")]
        [Route(nameof(EligibleBanks))]
        [Consumes("application/vnd.uk.co.vanquis.payit.eligiblebanks.list-v1+json")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<EligibleBank>))]
        public async Task<IActionResult> EligibleBanks()
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            var eligbleBanks = await _payItService.GetEligibleBanks();

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(eligbleBanks);
        }

        /// <summary>
        /// Initiates the payment.
        /// </summary>
        /// <param name="paymentInitiationModel">The payment initiation model.</param>
        /// <param name="paymentStatus" cref="PaymentStatus">The optional payment status.</param>
        /// <returns></returns>
        [HttpPost]
        [ApiVersion("1.0")]
        [Route(nameof(InitiatePayment))]
        [Consumes("application/vnd.uk.co.vanquis.payit.initiatepayment-v1+json")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(PaymentRequestResponse))]
        public async Task<IActionResult> InitiatePayment(PaymentInitiationModel paymentInitiationModel, PaymentStatus? paymentStatus = null)
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            if (!ModelState.IsValid)
            {
                LoggingHelper.LogError(_logger, Request, $"Invalid model {paymentInitiationModel.GetType().FullName} passed in. Request rejected");
                return BadRequest(ModelState.Values.SelectMany(e => e.Errors).Select(e => e.ErrorMessage));
            }

            if (string.IsNullOrWhiteSpace(paymentInitiationModel.AccountNumber) || paymentInitiationModel.AccountNumber.Length < 16)
            {
                return BadRequest($"{Request.Method} {Request.Path.Value}: {nameof(PaymentRequestModel.AccountNumber)} must be valid");
            }

            if (paymentInitiationModel.Amount < 0.01)
            {
                return BadRequest($"{Request.Method} {Request.Path.Value}: {nameof(PaymentRequestModel.Amount)} must be greater than 0.00");
            }

            // This is required for sandbox
            _payItService.AddFovIndicator(HttpContext, paymentStatus);

            var paymentRequestModel = new PaymentRequestModel
            {
                AccountNumber = paymentInitiationModel.AccountNumber,
                Amount = paymentInitiationModel.Amount,
                Reason = $"{paymentInitiationModel.AccountNumber.Substring(paymentInitiationModel.AccountNumber.Length - 12)}{DateTimeOffset.UtcNow.ToString("yyMMdd")}",
                RedirectUrl = _settings.RedirectionUrl
            };

            var paymentRequestResponse = await _payItService.InitiatePayment(paymentRequestModel);

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(paymentRequestResponse);
        }
    }
}