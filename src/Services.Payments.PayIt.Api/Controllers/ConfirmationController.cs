﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;
using Services.Payments.PayIt.Infrastructure.Models;
using Services.Payments.PayIt.Infrastructure.Models.Request;
using Services.Payments.PayIt.Infrastructure.Models.Response;

namespace Services.Payments.PayIt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfirmationController : ControllerBase
    {
        #region Properties

        private readonly ILogger<ConfirmationController> _logger;
        private readonly ITransactionsService _transactionsService;
        private readonly IWebhookService _webhookService;
        private readonly NatWestPayItConfigurationResource _natWestPayItConfigurationResource;

        private const string DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.fffK";

        #endregion Properties

        #region Constructors

        public ConfirmationController(ILogger<ConfirmationController> logger, IWebhookService webhookService, ITransactionsService transactionsService, NatWestPayItConfigurationResource natWestPayItConfigurationResource)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _webhookService = webhookService ?? throw new ArgumentNullException(nameof(webhookService));
            _transactionsService = transactionsService ?? throw new ArgumentNullException(nameof(transactionsService));
            this._natWestPayItConfigurationResource = natWestPayItConfigurationResource ?? throw new ArgumentNullException(nameof(natWestPayItConfigurationResource));
        }

        #endregion Constructors

        /// <summary>
        /// Redirection for NatWest PayIt when the transaction has been has been processed.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ContentResult))]
        public async Task<ContentResult> Get()
        {
            LoggingHelper.LogInformation(_logger, Request, "Redirection recieved");

            var loadingPage = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Templates\\loadingPage.html");
            var contentResult = new ContentResult()
            {
                ContentType = "text/html",
                Content = System.IO.File.Exists(loadingPage) ? System.IO.File.ReadAllText(loadingPage) : string.Empty
            };

            LoggingHelper.LogInformation(_logger, Request, "Redirection successful");

            return contentResult;
        }

        /// <summary>Webhook for NatWest PayIt to confirm transaction.</summary>
        /// <returns cref="ConfirmationResponseData"></returns>
        [HttpPost, Route("merchant-payments-confirm")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ConfirmationResponse))]
        public async Task<IActionResult> Post()
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            var paymentConfirmationModel = await GetVerifiedPayload<PaymentConfirmationModel>();
            string transactionId = Request.Headers["x-transaction-ID"];

            var payment = await ValidateAndUpdatePayment(transactionId, paymentConfirmationModel.PaymentUpdate);

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(new ConfirmationResponse
            {
                Data = new ConfirmationResponseData
                {
                    CreationDateTime = DateTime.UtcNow.ToString(DATETIME_FORMAT),
                    MerchantAcknowledgementId = Guid.NewGuid().ToString().ToLower().Remove(35, 1)
                }
            });
        }

        /// <summary>Webhook for NatWest PayIt to update confirmed transaction.</summary>
        /// <param name="PaymentReferenceId">The payment reference identifier.</param>
        /// <param name="TppReferenceId">The TPP reference identifier.</param>
        /// <returns cref="ConfirmationResponseData"></returns>
        [HttpPatch, Route("merchant-payments-confirm/PaymentReferenceId/{PaymentReferenceId}/TppReferenceId/{TppReferenceId}")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ConfirmationResponse))]
        public async Task<IActionResult> Patch(string PaymentReferenceId, string TppReferenceId)
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            var paymentConfirmationPatchModel = await GetVerifiedPayload<PaymentConfirmationPatchModel>();
            string transactionId = Request.Headers["x-transaction-ID"];

            var paymentConfirmationData = new PaymentConfirmationData
            {
                PaymentReferenceId = PaymentReferenceId,
                TppReferenceId = TppReferenceId,
                Status = paymentConfirmationPatchModel.Status,
                StatusDateTime = DateTimeOffset.UtcNow
            };

            var payment = await ValidateAndUpdatePayment(transactionId, paymentConfirmationData);

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(new ConfirmationResponse
            {
                Data = new ConfirmationResponseData
                {
                    CreationDateTime = DateTime.UtcNow.ToString(DATETIME_FORMAT),
                    MerchantAcknowledgementId = Guid.NewGuid().ToString().ToLower().Remove(35, 1)
                }
            });
        }

        #region Helper Methods

        private async Task<Payment> ValidateAndUpdatePayment(string transactionId, PaymentConfirmationData paymentConfirmationData)
        {
            var payment = await _transactionsService.GetPayment(transactionId);
            var currentPaymentStatus = !string.IsNullOrWhiteSpace(payment.Status) ? (PaymentStatus)Enum.Parse(typeof(PaymentStatus), payment.Status)
                : PaymentStatus.Undefined;

            if (payment == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, Request, $"Cannot find the payment {transactionId}");
            }

            if (currentPaymentStatus == PaymentStatus.PaymentApproved)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, Request, $"Cannot update payment {transactionId}, as it has already been approved");
            }

            payment = await _transactionsService.UpdatePayment(transactionId, paymentConfirmationData);

            return payment;
        }

        private async Task<T> GetVerifiedPayload<T>()
        {
            string requestBody;
            using (var streamReader = new StreamReader(Request.Body))
            {
                requestBody = await streamReader.ReadToEndAsync();
            }

            if (string.IsNullOrWhiteSpace(requestBody))
            {
                LoggingHelper.LogErrorAndThrowException(_logger, Request, "Request body cannot be empty");
            }

            var verifiedPayload = await _webhookService.VerifyPayload(Request.Headers["payit-signature"].ToString(), requestBody);
            if (!verifiedPayload)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, Request, "Payload cannot be accepted as the signature is invalid");
            }

            var data = JsonConvert.DeserializeObject<T>(requestBody);
            if (data == null)
            {
                LoggingHelper.LogErrorAndThrowException(_logger, Request, "Cannot deserialise, invalid request body");
            }

            return data;
        }

        private string DecodeString(string base64string)
        {
            if (string.IsNullOrWhiteSpace(base64string)) return string.Empty;
            return System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(base64string));
        }

        #endregion Helper Methods
    }
}