﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Helpers;
using Services.Payments.PayIt.Infrastructure.Models;

namespace Services.Payments.PayIt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        #region Propeties

        private readonly ILogger<TransactionsController> _logger;
        private readonly ITransactionsService _transactionsService;

        #endregion Propeties

        #region Constructors
        public TransactionsController(ILogger<TransactionsController> logger, ITransactionsService transactionsService)
        {
            _logger = logger;
            _transactionsService = transactionsService;
        }

        #endregion Constructors

        /// <summary>
        /// Get the specified transaction for the account.
        /// </summary>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <returns></returns>
        [HttpGet("{accountNumber}/{transactionId}")]
        [ApiVersion("1.0")]
        [Consumes("application/vnd.uk.co.vanquis.payit.transactions.list-v1+json")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Payment))]
        public async Task<IActionResult> Get(string accountNumber, string transactionId)
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            var transaction = await _transactionsService.GetPayment(transactionId);

            if (transaction.AccountNumber != accountNumber)
            {
                var error = $"Access denied. Transaction {transactionId} does not belong to account {accountNumber}";
                LoggingHelper.LogInformation(_logger, Request, error);
                return BadRequest(error);
            }

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(transaction);
        }

        /// <summary>
        /// Get a list of transactions for the account.
        /// </summary>
        /// <param name="accountNumber">The account number.</param>
        /// <returns></returns>
        [HttpGet("{accountNumber}")]
        [ApiVersion("1.0")]
        [Consumes("application/vnd.uk.co.vanquis.payit.transactions.list-v1+json")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(List<Payment>))]
        public async Task<IActionResult> Get(string accountNumber)
        {
            LoggingHelper.LogInformation(_logger, Request, "Request recieved");

            var transactions = await _transactionsService.GetPayments(accountNumber);

            LoggingHelper.LogInformation(_logger, Request, "Request successful");

            return Ok(transactions);
        }
    }
}