using System.IO;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using Newtonsoft.Json.Converters;

using Serilog;

using Services.Payments.PayIt.Core.Resources;
using Services.Payments.PayIt.Domain.Services.Implementations;
using Services.Payments.PayIt.Domain.Services.Interfaces;
using Services.Payments.PayIt.Infrastructure.Context.Implementations;
using Services.Payments.PayIt.Infrastructure.Context.Interfaces;
using Services.Payments.PayIt.Infrastructure.Extensions;

namespace Services.Payments.PayIt.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private Database _cosmosDatabase;

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            if (env.IsProduction())
            {
                var builtConfig = builder.Build();

                var azureServiceTokenProvider = new AzureServiceTokenProvider();
                var keyVaultClient = new KeyVaultClient(
                    new KeyVaultClient.AuthenticationCallback(
                        azureServiceTokenProvider.KeyVaultTokenCallback));

                builder.AddAzureKeyVault(
                    $"https://{builtConfig["KeyVaultName"]}.vault.azure.net/",
                    keyVaultClient,
                    new DefaultKeyVaultSecretManager());
            }

            _configuration = builder.Build();

            // add connection to cosmos database and inject into services in DependencyInjections()
            Task.Run(async () =>
            {
                _cosmosDatabase = await new CosmosClient(_configuration.GetSection("Database:ConnectionString").Value)
                    .CreateDatabaseIfNotExistsAsync(_configuration.GetSection("Database:DatabaseName").Value);
            }).Wait();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //setup application insights
            services.AddApplicationInsightsTelemetry();
            services.AddCors();
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.Converters.Add(new StringEnumConverter()));

            // application dependency injections
            DependencyInjections(services);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Natwest Payit", Version = "v1" });
                c.IncludeXmlComments(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Services.Payments.PayIt.Api.xml"));
            });
            services.AddSwaggerGenNewtonsoftSupport();

            //configure versioning
            ConfigureVersioning(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var log = new LoggerConfiguration().ReadFrom.Configuration(_configuration).CreateLogger();
            loggerFactory.AddSerilog(log);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Custom error handling configured
            app.ConfigureCustomExceptionMiddleware();

            app.UseHttpsRedirection();
            app.UseRouting();

            // Global CORS policy, this is handled by the APIM in Azure
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payit Api V1");
            });
        }

        private static void ConfigureVersioning(IServiceCollection services)
        {
            services.AddApiVersioning(ver =>
            {
                ver.ReportApiVersions = true;
                ver.AssumeDefaultVersionWhenUnspecified = true;
                ver.DefaultApiVersion = new ApiVersion(1, 0);
                ver.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
            });
        }

        private void DependencyInjections(IServiceCollection services)
        {
            services.AddSingleton(_cosmosDatabase);
            services.AddSingleton(provider => _configuration.GetSection("ApplicationInsights").Get<ApplicationInsightsSettingsResource>());
            services.AddSingleton(provider => _configuration.GetSection("NatWestPayItConfiguration").Get<NatWestPayItConfigurationResource>());

            services.AddScoped<IDbContext, DbContext>();
            services.AddScoped<ITransactionsService, TransactionsService>();
            services.AddHttpClient<IPayItService, PayItService>();
            services.AddTransient<IWebhookService, WebhookService>();
        }
    }
}
